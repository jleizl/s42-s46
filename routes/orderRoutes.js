const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');


// Non-admin User checkout(Create Order)

router.post("/createOrder", auth.verify, (req, res) => {

const userData = auth.decode(req.headers.authorization)

  	orderController.createOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});

module.exports = router; 


// stretch goal
// retrieve authenticated user's order

router.get("/userOrder/:userId", auth.verify, (req, res) => {

	
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	orderController.userOrder({id: userData.id}).then(resultFromController => res.send(resultFromController));
});


// retrieve all orders (admin only)
// add to cart
	// added products
	// change product from cart
	// subtotal for each item
	// total price for all items


	 /*let data = {
		userId: req.body.userId, 
		productId: req.body.productId,
		quantity: req.params.quantity,
		totalAmount: req.totalAmount,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}*/