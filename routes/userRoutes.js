const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User login/authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then((resultFromController) => res.send(resultFromController))
});

// Retrieve User Details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Retrieve All User Details (admin only)
router.get("/allUsers", auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.getAllUsers(data).then(resultFromController => res.send(resultFromController));
});


// set user as admin(admin only)

router.put("/setUserAsAdmin/:userId", auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	
	userController.setUserAsAdmin(data).then(resultFromController => res.send(resultFromController))

});



module.exports = router;


