const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');


// Non-admin User checkout(Create Order) function

 module.exports.createOrder = (reqBody, userData) => {

     return User.findById(userData.userId).then(result => {
        if (userData.isAdmin) {
            return "Not allowed to create order"
        } else {

            let newOrder = new Order({
                userId: userData.userId,
                productId: reqBody.productId,
                productName: reqBody.productName,
                quantity: reqBody.quantity,
                totalAmount: reqBody.totalAmount
            });

            return newOrder.save().then((order, error) => {

                if (error) {
                    return false

                } else {
                    return "Order created successfully"
                }
            })
        } 
    })
};


// retrieve authenticated user's order
module.exports.userOrder = (userData) => {
    return User.findById(userData.id).then(result => {
        console.log(orderData)
        if (result == null) {
            return false
        } else {
            console.log(result)
            return result;
        }
    })
};

   