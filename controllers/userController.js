const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// // User Registration function
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        userName: reqBody.userName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if (error) {
            return false
        } else {
            return true
        }
    })
};

// User login function
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) }
            } else {
                return false
            }
        }
    })
};


// user authentication function
module.exports.getProfile = (userData) => {
    return User.findById(userData.id).then(result => {
        console.log(userData)
        if (result == null) {
            return false
        } else {
            console.log(result)
            result.password = "**********";
            return result;
        }
    })
};

// Retrieve All User Details (admin only)

module.exports.getAllUsers = (data) => {
    return User.find({ isAdmin: false }).then((result, error) => {
        if (data.isAdmin == false) {
            return "You are not an Admin"
        } else {
            console.log(result)
            result.password = "**********";
            return result;
        }
    })
}


// set user as admin(admin only)
module.exports.setUserAsAdmin = (data) => {

    return User.findById(data.userId).then((result, error) => {

    if(data.isAdmin === false) {
            return "access denied"
        } else {
       result.isAdmin = true;

            return result.save().then((setUserAsAdmin, error) => {

                if(error) {

                    return false;

                } else {

                    return "User is now an Admin";
                }
            })
        } 
    })
};