const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();

const port = process.env.PORT || 4000;


// Middlewares:
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI:
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

// Mongoose Connection

mongoose.connect(`mongodb://jleizl93:admin123@ac-floig6u-shard-00-00.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-01.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-02.2zb8dt2.mongodb.net:27017/s42-s46?ssl=true&replicaSet=atlas-12lysg-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection


db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.log('Connected to MongoDB!') )
app.listen(port, () => console.log(`API is now online at port: ${port}`));